import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/*
* Create tests
*/
public class TestClass extends TestCase {
    public TestClass(String testName) {
        super(testName);
    }

    public void test1() {
        RhinoEngine t1 = new RhinoEngine("5+6");
        assertTrue(t1.run().equals("5+6 = 11.0"));
    }

    public void test2() {
        RhinoEngine t2 = new RhinoEngine(" sin(42)");
        assertTrue(t2.run().equals(" sin(42) = 0.6691306063588582"));
    }

    public void test3() {
        RhinoEngine t3 = new RhinoEngine("(57+1/tg(53))*sqrt(54-cos(23))");
        assertTrue(t3.run().equals("(57+1/tg(53))*sqrt(54-cos(23)) = 420.76742174571075"));
    }

    public void test4() {
        RhinoEngine t3 = new RhinoEngine("pow (5, (2*2))");
        assertTrue(t3.run().equals("pow (5, (2*2)) = 625.0"));
    }
}

class Go {
    public static void main(String[] args) {
        TestRunner runner = new TestRunner();
        TestSuite suite = new TestSuite();
        suite.addTest(new TestClass("test1"));
        suite.addTest(new TestClass("test2"));
        suite.addTest(new TestClass("test3"));
        suite.addTest(new TestClass("test4"));
        runner.doRun(suite);
    }
}

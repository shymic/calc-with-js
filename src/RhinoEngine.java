import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class RhinoEngine {
    private ScriptEngineManager mgr;
    private ScriptEngine engine;
    private String str;

    RhinoEngine(String inputString) {
        mgr = new ScriptEngineManager();
        engine = mgr.getEngineByName("JavaScript");
        str = inputString;
        try {
            functionInitialization();
        } catch (ScriptException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void functionInitialization() throws ScriptException {
        engine.eval("function radians(deg){return deg*(Math.PI/180);}");
        engine.eval("function sin(a){ return Math.sin(radians(a)); }");
        engine.eval("function cos(a){ return Math.cos(radians(a)); }");
        engine.eval("function tg(a){ return Math.tan(radians(a)); }");
        engine.eval("function log(a){ return Math.log(a); }");
        engine.eval("function sqrt(a){ return Math.sqrt(a); }");
        engine.eval("function pow(a, b){ return Math.pow(a, b); }");
    }

    public String run() {
        try {
            engine.eval(new String("a = " + str));
        } catch (ScriptException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return str + " = " + engine.get("a").toString();
    }
}